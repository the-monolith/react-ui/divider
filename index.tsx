import { component, React, Style } from 'local/react/component'

export const defaultDividerStyle: Style = {}

export const Divider = component
  .props<{ style?: Style }>({
    style: defaultDividerStyle
  })
  .render(({ style }) => {
    return <div style={style} />
  })
